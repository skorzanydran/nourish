import {createI18n} from "vue-i18n";
import en from "@/i18n/locales/en.json"
import ru from "@/i18n/locales/ru.json"

const messages = {
    en,
    ru
}

const i18n = createI18n({
    locale: 'en',
    fallbackLocale: 'en',
    globalInjection: true,
    messages: messages
});

export default i18n
