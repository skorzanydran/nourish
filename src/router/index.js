import {createRouter, createWebHistory} from 'vue-router';

let router = createRouter({
    history: createWebHistory(),
    routes: [
        {
            path: '/',
            redirect: 'home'
        },
        {
            path: '/home',
            name: 'home',
            component: () => import('@/views/main/MainPage.vue'),
        },
        {
            path: '/dashboard',
            name: 'dashboard',
            component: () => import('@/views/dashboard/DashboardPage.vue'),
            children: [
                {
                    path: '',
                    name: 'summary',
                    component: () => import('@/views/dashboard/summary/SummaryPage.vue')
                },
                {
                    path: '/profile',
                    name: 'profile',
                    component: () => import('@/views/dashboard/profile/ProfilePage.vue')
                }
            ]
        }
    ]
});

export default router
